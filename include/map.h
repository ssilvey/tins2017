//
//	Map.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#ifndef __tsl7_map__
#define __tsl7_map__

#include "allegro_tiled.h"

class Map {
    public:
        Map();
        Map(const char *path, const char *filename);

        ALLEGRO_MAP *aMap;

        bool isTileValid(int x, int y);
        void Draw(int x, int y);
        ALLEGRO_MAP_TILE* getTile(int x, int y);
    private:
        
};
#endif
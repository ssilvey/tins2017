//
//	constants.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#ifndef __tsl7_constants__
#define __tsl7_constants__

/// Misc
#define FPS                         60
#define Verison                     1.1
#define MAX_DANCE_STEPS             16

/// Colors
#define COLOR_THEME_ONE             al_map_rgba_f(0.1569, 0.1255, 0.3451, 1)
#define COLOR_THEME_SIX             al_map_rgba_f(0.6275, 0.5294, 0.8431, 1)

/// Dimensions
#define SCREEN_W                    1024
#define SCREEN_H                    768

/// Games
#define MAX_TILE_COLUMNS            12
#define MAX_TILE_ROWS               12
#define MAX_ALIENS                  8
#define MAX_HFACTORS                8
#define MAX_PATHOGENS_IN_SYSTEM     8
#define MAX_PATHOGENS_AVAILABLE     24
#define PATHOGEN_TYPES              16

/// Buttons
#define BUTTON_ADJUST               100
#define BUTTON_PAUSE                101
#define BUTTON_LAB                  102


/// Strings



#endif
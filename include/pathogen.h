//
//	pathogen.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#ifndef __tsl7_pathogen__
#define __tsl7_pathogen__

#include "health.h"

#include <allegro5/allegro.h>


class Pathogen {
    public:
        Pathogen();
        ~Pathogen();

        Health *health;
        ALLEGRO_BITMAP *image;

        void Print();
        void LoadImage(int ndx);
        void Draw();

        double x,y;
        double alpha;
    private:
        
};
#endif
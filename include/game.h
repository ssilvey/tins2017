
//
//	game.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#ifndef __tsl7_game__
#define __tsl7_game__

#include "basescreen.h"
#include "constants.h"
#include "widgetz/widgetz.h"
#include "titlescreen.h"
#include "labscreen.h"
#include <allegro5/allegro_audio.h>

enum MYKEYS
{
    KEY_UP,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT, 
    KEY_SHIFT,
    KEY_SPACE
};


class Game {
public:
    static Game& getInstance()
    {
        static Game instance;
        return instance;
    }
    
    int Setup(ALLEGRO_DISPLAY *display);
    void Cleanup();
    void Run();
    void Draw();
    void Update();
    void ProcessInput(int unichar);
    void SwitchScreenMode();
    void PlaySong(int i);

    ALLEGRO_FONT *baseFont;
    ALLEGRO_TIMER *timer;
    ALLEGRO_EVENT_QUEUE *event_queue;
    ALLEGRO_DISPLAY *display;
    ALLEGRO_JOYSTICK *joystick;
    BaseScreen *currentScreen;
    WZ_WIDGET *gui;
    WZ_DEF_THEME theme;
    WZ_WIDGET *wgt;
    float guisize;
    bool redraw;
    bool done;
    bool key[6];

    TitleScreen *titleScreen;
    LabScreen *labScreen;
    
    
private:
    Game() {};
    double fixed_dt;
    double old_time;
    double game_time;
    double start_time;
    int refresh_rate;

    ALLEGRO_SAMPLE *song1, *song2, *song3;

};

#endif 

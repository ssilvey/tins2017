//
//	Player.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 21st, 2017
//

#ifndef __tsl7_player__
#define __tsl7_player__

#include "algif.h"
#include "map.h"

class Player {
    public:
        Player();
        ~Player();

        void Update(Map *map, double dt);
        void Draw();

        ALGIF_ANIMATION *avatar_idle;
        ALGIF_ANIMATION *avatar_running;
        ALGIF_ANIMATION *avatar_running_fast;
        double x,y;
        bool flip;
        bool running, fast;
        
    private:
        
};
#endif
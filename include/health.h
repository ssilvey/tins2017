//
//	Health.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#ifndef __tsl7_health__
#define __tsl7_health__

enum HFACTORS
{
    NEG_GUT,
    NEG_HEART,
    NEG_BRAIN,
    NEG_TEMP, 
    POS_GUT,
    POS_HEART,
    POS_BRAIN,
    POS_TEMP
};

enum HFACTORS2
{
    H_GUT,
    H_HEART,
    H_BRAIN,
    H_TEMP
};

class Health {
    public:
        Health();
        Health(int value);
        ~Health();

        void Adjust(int factor, int amount);
        void Impact(int factor, int amount);

        int gut;
        int brain;
        int heart;
        int temp;
    private:
        
};
#endif
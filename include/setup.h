//
//  setup.h
//  TINS2017 - tsl7
//  Created by Steven Silvey
//  October 20th, 2017
//

#ifndef __tsl7_setup__
#define __tsl7_setup__

int setup();

#endif

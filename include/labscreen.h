//
//	LabScreen.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#ifndef __tsl7_labscreen__
#define __tsl7_labscreen__

#include "basescreen.h"
#include "algif.h"
#include "alien.h"
#include "allegro_tiled.h"
#include "player.h"
#include "map.h"
#include "constants.h"

class Pathogen;
class PathogenSlot;
class Health;
static double dance_steps[MAX_DANCE_STEPS] = {1.0f, 1.0f, 0.5f, 1.0f, 1.0f, 0.5f, 0.25f, 0.25f, 0.25, 0.25, 0.1, 0.1, 0.1, 0.1, 0.5, 1.0f };

class LabScreen: public BaseScreen {
    public:
        LabScreen();
        ~LabScreen();

        void Setup();
        void Update(ALLEGRO_EVENT ev, double dt);
        void Draw();
        void DrawOnGUI();

        Map *map;
        Health *health;
        WZ_WIDGET *dialogueBox;
        WZ_TEXTBOX *dialogueMessage;
        WZ_TEXTBOX *wzVitals;
        WZ_TEXTBOX *wzSystem;

        WZ_WIDGET *wzLab;
        

        Alien *arrAliens;
        Alien *alienSelected;
        Player *player;
        PathogenSlot *arrPathogenSlots;

        PathogenSlot *arrAvailablePathogenSlots;
        PathogenSlot *slotA;
        PathogenSlot *slotB;

        ALLEGRO_BITMAP *imgA;
        ALLEGRO_BITMAP *imgB;
        ALLEGRO_BITMAP *imgC;

        Pathogen *pathogenA;
        Pathogen *pathogenB;
        Pathogen *pathogenC;


    private:
        void StartDialogue();
        void EndDialogue();
        void BalanceSystem();
        void UpdateSystem(double dt);
        string DetermineStatus(int currentFactor, int changeInFactor);
        void Pause(ALLEGRO_EVENT ev, double dt);
        void MergePathogens();

        double timer_health;
        string strGutStatus, strHeartStatus, strBrainStatus, strTempStatus;
        bool paused;
        double timer_dance;
        double timer_dance_duration;
        int dance_step_ndx;

        double timer_merging;

        bool inTheLab;
        bool inTheEV;
        bool mergingPathogens;

        int quads[4];
        
};
#endif
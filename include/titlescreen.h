//
//	TitleScreen.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#ifndef __tsl7_titlescreen__
#define __tsl7_titlescreen__

#include "basescreen.h"


class TitleScreen:public BaseScreen {
    public:
        TitleScreen();

        void Setup();
        void Update(ALLEGRO_EVENT ev, double dt);
        

    private:
        
};
#endif
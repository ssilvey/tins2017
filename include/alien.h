//
//	Alien.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 21st, 2017
//

#ifndef __tsl7_alien__
#define __tsl7_alien__

#include "algif.h"
#include <string>
using namespace std;


class Map;
class Health;

enum DIRECTIONS
{
    DIR_NONE,
    DIR_UP,
    DIR_DOWN,
    DIR_LEFT,
    DIR_RIGHT
};

class Alien {
    public:
        Alien();
        ~Alien();

        ALGIF_ANIMATION *avatar_walk;
        ALGIF_ANIMATION *avatar_idle;

        void Draw(bool selected);
        void Update(Map *map, double dt);
        double x,y;
        bool flip;
        int dir;
        double dir_timer, dir_duration;
        string msg;
        bool talking;

        Health *health;
        bool idle;
        
    private:
        
        
};
#endif
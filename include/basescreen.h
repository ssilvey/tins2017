//
//	BaseScreen.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#ifndef __tsl7_basescreen__
#define __tsl7_basescreen__

#include "widgetz/widgetz.h"

class BaseScreen {
    public:
        BaseScreen();

        void Setup();
        virtual void Update(ALLEGRO_EVENT ev, double dt);
        virtual void Draw();
        virtual void DrawOnGUI();

        WZ_WIDGET *gui;

    private:
        
};
#endif
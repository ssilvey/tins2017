//
//	PathogenSlot.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#ifndef __tsl7_pathogenslot__
#define __tsl7_pathogenslot__

#include <allegro5/allegro.h>
#include "pathogen.h"

class PathogenSlot {
    public:
        PathogenSlot();
        ~PathogenSlot();

        ALLEGRO_BITMAP *imgSlot;
        Pathogen *pathogen;
        int x,y;

        void Draw();
        void Update(ALLEGRO_EVENT ev, double dt);

        bool big;
        bool selectable;
        bool selected;

    private:
        
};
#endif
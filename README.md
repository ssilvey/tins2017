# README #
# The Tragedy at Space Lab #7 
# Author:  Steven Silvey (aka Onewing / SuperSilvey)

Overview

### What is this repository for? ###

* Quick summary
* Version: 1.1
* Tested with Allegro 5.2.2
This repo houses commits for my TINS 2017 entry, The Tragedy at Space Lab #7.  

### TINS RULES ###
  * **Genre**
  Theme:  Doctors & Health
  Status:  Done! - The aliens in the lab each have health based on four factors:  gut, heart, brain and temperature.  As the environment gets worse, these health factors deterioate.

  * **Artistic**
  Rule 1: Pause mode with dancing characters
  Status:  Done! - Press the pause button.  Enjoy!

  Rule 2: Silly weapon
  Status:  Act of YouTube! - See here:  www.solo-games.org/games/tins2017/tins15.mov 

  * **Technical**
  Rule 1:  Morphing Effect
  Status:  Done! - Go to the lab!

### Installation ###

* Summary of set up
* Dependencies
  ** Widgetz **
  1. Build the library from here: https://github.com/SiegeLord/WidgetZ
  2. Move the created libwidgetz.a file to the lib folder

  ** libalgif **
  1.  Get the library here:  https://github.com/allefant/algif5
  2.  Run the following:  
  gcc -c *.c
  ar -rcs libalgif.a *.o
  3. Move the created libalgif.a file to the lib folder

  ** allegro_tiled **
  1.  Get the library here:  https://github.com/dradtke/allegro_tiled
  2.  Review the build instructions there
  3. Move the created liballegro_tiled.a to the lib folder

  NOTE:
  On OS X, I had to install via homebrew pkconfig.  


* Build tsl7
  g++ -g ./src/*.cpp -Wall -ansi -o ./bin/tsl7 -L/usr/lib/ -L./lib/ -I/usr/include/allegro5 -I./include -lallegro -lallegro_font -lallegro_color -lallegro_ttf -lallegro_image -lallegro_dialog -lallegro_main -lallegro_audio -lallegro_acodec -lallegro_primitives -lwidgetz -lalgif -lallegro_tiled-lxml2 -lglib-2.0 -lz

  The executable will be in the bin folder.

### Who do I talk to? ###

* Steven Silvey <supersilvey@gmail.com>

### Code Reuse ###
- Code from my (Onewing) entry (Elders of Ethos) for Speedhack 2015:  http://www.speedhack.allegro.cc/entrants
- Using the Widgetz GUI from SiegeLord:  https://github.com/SiegeLord/WidgetZ
- Using algif5:  http://allegro5.org/algif5/ 

### Assets ###
- Using the Space Station Tileset from here:  https://braqoon.deviantart.com/art/Space-Station-Tileset-195196369
- Land Monster Sprite Sheet:  http://bevouliin.com/game-villain-land-monster-sprite-sheets/
- 16 Alien Marbles for Game Developers:  http://bevouliin.com/16-alien-marbles-game-developers/
- Alien Game Character Sprite Sheets:  http://bevouliin.com/alien-game-character-sprite-sheets/
- Got free music from here:  https://www.dl-sounds.com/royalty-free/minimal-funky-loop/

### Tools Used ###
- Adobe Photoshop
- Tiled - http://doc.mapeditor.org/en/latest/manual/introduction/#getting-started
- Made gifs here: http://gifmaker.org/
- Converted music to ogg:  https://audio.online-convert.com/convert-to-ogg

//
//	LabScreen.h
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#include <stdio.h>
#include "labscreen.h"
#include "game.h"
#include "allegro5/allegro_image.h"
#include "health.h"
#include "pathogen.h"
#include "pathogenslot.h"
#include <vector>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Life Cycle

LabScreen::LabScreen()
{
    this->paused = false;
    this->timer_dance = 0;
    this->dance_step_ndx = 0;
    this->timer_dance_duration = dance_steps[this->dance_step_ndx];

    this->imgA = al_create_bitmap(64, 64);
    this->imgB = al_create_bitmap(64, 64);
    this->imgC = al_create_bitmap(64, 64);
    this->mergingPathogens = false;

    this->inTheEV = false;
}

LabScreen::~LabScreen()
{   
    delete this->arrAliens;
    delete this->arrPathogenSlots;
    delete this->health;
    delete this->player;
}

void LabScreen::Setup()
{
    Game *game = NULL;

    memset(&game->getInstance().theme, 0, sizeof(game->getInstance().theme));
    memcpy(&game->getInstance().theme, &wz_def_theme, sizeof(game->getInstance().theme));
    game->getInstance().theme.font = game->getInstance().baseFont;
    game->getInstance().theme.color1 = COLOR_THEME_ONE;
    game->getInstance().theme.color2 = COLOR_THEME_SIX;

    this->gui = wz_create_widget(0, 0, 0, -1);
    wz_set_theme(this->gui, (WZ_THEME *)&game->getInstance().theme);

    // wz_create_fill_layout(this->gui, 50 * game->getInstance().guisize, 50 * game->getInstance().guisize, 300 * game->getInstance().guisize, 450 * game->getInstance().guisize, 50 * game->getInstance().guisize, 20 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_CENTRE, al_ustr_new("Welcome to WidgetZ!"), 1, -1);
    // wz_create_toggle_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Toggle 1"), 1, 1, 5);
    // wz_create_toggle_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Toggle 2"), 1, 1, 6);
    // wz_create_toggle_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Toggle 3"), 1, 1, 7);
    // wz_create_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Switch Themes"), 1, 666);
    // game->getInstance().wgt = (WZ_WIDGET *)wz_create_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Quit"), 1, 1);
    // wz_set_shortcut(game->getInstance().wgt, ALLEGRO_KEY_Q, ALLEGRO_KEYMOD_CTRL);
    // wz_create_fill_layout(this->gui, 350 * game->getInstance().guisize, 50 * game->getInstance().guisize, 300 * game->getInstance().guisize, 450 * game->getInstance().guisize, 50 * game->getInstance().guisize, 20 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_CENTRE, al_ustr_new("Scroll Bars:"), 1, -1);
    // wz_create_fill_layout(this->gui, 650 * game->getInstance().guisize, 50 * game->getInstance().guisize, 300 * game->getInstance().guisize, 450 * game->getInstance().guisize, 20 * game->getInstance().guisize, 20 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_CENTRE, al_ustr_new("Edit Box:"), 1, -1);
    // game->getInstance().wgt = (WZ_WIDGET *)wz_create_editbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Type here..."), 1, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("A textbox with a lot of text"
    //                                                                                                                                                  " in it. Also supports new lines:\n\nNew paragraph.\n"
    //                                                                                                                                                  "Also supports unicode:\n\n"
    //                                                                                                                                                  "Привет"),
    //                   1, -1);

    /// Lab
    wz_create_button(this->gui, 784,650,224,40,al_ustr_new("Lab"),1,BUTTON_LAB);
    
    /// Pause Button
    wz_create_button(this->gui, 784,700,224,40,al_ustr_new("Pause"),1,BUTTON_PAUSE);
    

    // WZ_FILL_LAYOUT* wz_create_fill_layout(WZ_WIDGET* parent, float x, float y, float w, float h, float hspace, float vspace, int halign, int valign, int id);
    this->dialogueBox = (WZ_WIDGET *)wz_create_fill_layout(this->gui, 0, 0, 200, 100, 60, 20, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    this->dialogueMessage = wz_create_textbox(this->dialogueBox, 10, 10, 180, 80, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("Welcome to WidgetZ!"), 1, -1);
    wz_show(this->dialogueBox, 0);

    /// Patient
    WZ_WIDGET *wgt = (WZ_WIDGET *)wz_create_fill_layout(this->gui, 784, 16, 224, 150, 0, 0, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    wz_create_textbox(wgt, 10, 10, 204, 80, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, al_ustr_new("**PATIENT**"), 1, -1);
    wz_create_textbox(wgt, 10, 40, 204, 100, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("Gut:\nHeart:\nBrain:\nTemp:"), 1, -1);
    this->wzVitals = wz_create_textbox(wgt, 80, 40, 280, 100, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("N/A\nN/A\nN/A\nN/A"), 1, -1);

    /// ENV CONTROL
    WZ_WIDGET *wgt2 = (WZ_WIDGET *)wz_create_fill_layout(this->gui, 784, 182, 224, 220, 0, 0, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    wz_create_textbox(wgt2, 10, 10, 204, 80, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, al_ustr_new("**ENV CONTROL**"), 1, -1);
    wz_create_button(wgt2, 10,150,204,60,al_ustr_new("Adjust"),1,BUTTON_ADJUST);

    /// System
    WZ_WIDGET *wgt3 = (WZ_WIDGET *)wz_create_fill_layout(this->gui, 784, 418, 224, 150, 0, 0, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    wz_create_textbox(wgt3, 10, 10, 204, 80, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, al_ustr_new("**SYSTEM**"), 1, -1);
    wz_create_textbox(wgt3, 10, 40, 204, 100, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("Gut:\nHeart:\nBrain:\nTemp:"), 1, -1);
    this->wzSystem = wz_create_textbox(wgt3, 80, 40, 280, 100, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("Normal\nImproving\nCritical\nDecaying"), 1, -1);

    /// Lab
    this->wzLab = (WZ_WIDGET *)wz_create_fill_layout(this->gui, 100, 100, 558, 500, 0, 0, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    wz_create_textbox(this->wzLab, 10, 10, 204, 80, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, al_ustr_new("**Welcome to the Lab**"), 1, -1);
    wz_show(this->wzLab, 0);
    this->arrAvailablePathogenSlots = new PathogenSlot[MAX_PATHOGENS_AVAILABLE];
    this->slotA = new PathogenSlot;
    this->slotB = new PathogenSlot;
    this->slotA->x = 270;
    this->slotA->y = 200;
    this->slotA->big = true;
    this->slotA->selectable = true;
    this->slotB->x = 470;
    this->slotB->y = 200;
    this->slotB->big = true;
    this->slotB->selectable = true;

    this->inTheLab = false;
    int ax = 0;
    int ay = 0;
    for(int i = 0; i < MAX_PATHOGENS_AVAILABLE; i++)
    {
        this->arrAvailablePathogenSlots[i].x = 108 + ax;
        this->arrAvailablePathogenSlots[i].y = 352 + ay;
        this->arrAvailablePathogenSlots[i].big = true;
        this->arrAvailablePathogenSlots[i].selectable = true;
        ax += 64 + 4;
        if(ax >= 68*8) {
            ax = 0;
            ay += 64 + 4;
        }
    }

    
    

    wz_register_sources(this->gui, game->getInstance().event_queue);
    
    al_find_resources_as(RELATIVE_TO_CWD);
    this->map = new Map("resources/maps", "sl_room3.tmx");
    this->player = new Player();
    this->health = new Health();
    this->health->gut = 40;
    this->health->heart = 40;
    this->health->brain = 40;
    this->health->temp = 40;
    this->timer_health = 0;

    this->arrAliens = new Alien[MAX_ALIENS];
    this->alienSelected = NULL;

    this->arrPathogenSlots = new PathogenSlot[MAX_PATHOGENS_IN_SYSTEM];
    int px = 0;
    int py = 0;
    for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++)
    {
        this->arrPathogenSlots[i].x = 810 + px;
        this->arrPathogenSlots[i].y = 232 + py;

        px += 40 + 4;
        if(px >= 44*4) {
            px = 0;
            py += 40 + 4;
        }
    }

    int x, y;
    for (int i = 0; i < MAX_ALIENS; i++)
    {
        x = 0;
        y = 0;
        while (!this->map->isTileValid(x, y))
        {
            x = arc4random() % MAX_TILE_COLUMNS;
            y = arc4random() % MAX_TILE_ROWS;
            this->arrAliens[i].x = x * 64;
            this->arrAliens[i].y = y * 64 - 18;
        }
    }

    BalanceSystem();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public Functions

void LabScreen::Update(ALLEGRO_EVENT ev, double dt)
{
    BaseScreen::Update(ev, dt);
    Game *game = NULL;
    if(this->paused) 
    {
        game->getInstance().redraw = true;
        Pause(ev, dt);
        return;
    }

    if(this->inTheLab) {
        PathogenSlot *selected = NULL;
        for(int i = 0; i < MAX_PATHOGENS_AVAILABLE; i++) {
            this->arrAvailablePathogenSlots[i].Update(ev, dt);
            if(this->arrAvailablePathogenSlots[i].selected) {
                selected = &this->arrAvailablePathogenSlots[i];
            }
        }

        if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
        {
            if(selected && !this->mergingPathogens) {
                if(!this->slotA->pathogen) {
                    this->slotA->pathogen = selected->pathogen;
                }
                else {
                    this->slotB->pathogen = selected->pathogen;
                    this->mergingPathogens = true;
                    MergePathogens();
                }
            }
        }

        if(this->mergingPathogens) {
            this->pathogenA->x += 128.0 * dt;
            this->pathogenB->x -= 128.0 * dt;
            if(this->pathogenA->x > 370) {
                this->pathogenA->x = 370;
            }
            if(this->pathogenB->x < 370) {
                this->pathogenB->x = 370;
            }

            this->timer_merging += dt;
            if(this->timer_merging > 8.0f) {
                this->timer_merging = 0;
                this->mergingPathogens = false;
                
                for(int i = 0; i<MAX_PATHOGENS_AVAILABLE; i++) {
                    if(!this->arrAvailablePathogenSlots[i].pathogen) {
                        this->arrAvailablePathogenSlots[i].pathogen = this->pathogenC;
                        break;
                    }
                }

                delete this->pathogenA;
                delete this->pathogenB;
                this->imgA = al_create_bitmap(64, 64);
                this->imgB = al_create_bitmap(64, 64);
                this->imgC = al_create_bitmap(64, 64);
                
            }
        }
    }

    if(this->inTheEV) {
        PathogenSlot *selected = NULL;
        bool add = true;
        int ndxAvail = -1;
        int ndxENV = -1;
        for(int i = 0; i < MAX_PATHOGENS_AVAILABLE; i++) {
            this->arrAvailablePathogenSlots[i].Update(ev, dt);
            if(this->arrAvailablePathogenSlots[i].selected) {
                selected = &this->arrAvailablePathogenSlots[i];
                ndxAvail = i;
            }
        }
        for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++) {
            this->arrPathogenSlots[i].Update(ev, dt);
            if(this->arrPathogenSlots[i].selected) {
                selected = &this->arrPathogenSlots[i];
                add = false;
                ndxENV = i;
            }
        }

        if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
        {
            if(selected) {
                if(add) {
                    for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++) {
                        if(!this->arrPathogenSlots[i].pathogen) {
                            this->arrPathogenSlots[i].pathogen = selected->pathogen;
                            this->arrAvailablePathogenSlots[ndxAvail].pathogen = NULL;
                            break;
                        }
                    }
                }
                else {
                    for(int i = 0; i < MAX_PATHOGENS_AVAILABLE; i++) {
                        if(!this->arrAvailablePathogenSlots[i].pathogen) {
                            this->arrAvailablePathogenSlots[i].pathogen = selected->pathogen;
                            this->arrPathogenSlots[ndxENV].pathogen = NULL;
                            break;
                        }
                    }
                }
            }
            
        }
    }

    if (ev.type == ALLEGRO_EVENT_TIMER)
    {
        game->getInstance().redraw = true;
    }
    else if (ev.type == WZ_BUTTON_PRESSED)
    {
        switch ((int)ev.user.data1)
        {
        case 1:
        {
            break;
        }
        case BUTTON_LAB:
        {
            this->inTheLab = !this->inTheLab;
            if(this->inTheLab) {
                wz_show(this->wzLab, 1);
            }
            else {
                wz_show(this->wzLab, 0);
            }
            break;
        }
        case BUTTON_ADJUST:
        {
            this->inTheEV = !this->inTheEV;
            if(this->inTheEV) {
                wz_show(this->wzLab, 1);
                for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++) {
                    this->arrPathogenSlots[i].selectable = true;
                }
            }
            else {
                wz_show(this->wzLab, 0);
                for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++) {
                    this->arrPathogenSlots[i].selectable = false;
                }
            }
            break;
        }
        case BUTTON_PAUSE:
            this->paused = !this->paused;
            if(this->paused)
            {
                game->getInstance().PlaySong(2);
                this->player->flip = true;
                this->player->running = true;
                this->player->fast = false;

                this->timer_dance = 0;
                this->dance_step_ndx = 0;
                this->timer_dance_duration = dance_steps[this->dance_step_ndx];
                
                for(int i = 0; i < MAX_ALIENS; i++) {
                    this->arrAliens[i].flip = false;
                    this->arrAliens[i].idle = false;
                }
        
                return;
            }
            
            
            break;
        }
    }
    else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
    {
        if (ev.keyboard.keycode == ALLEGRO_KEY_SPACE)
        {
            this->StartDialogue();
        }
        else
        {
            this->EndDialogue();
        }
    }
    else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
    {
        if (ev.joystick.button == 1)
        {
            this->StartDialogue();
        }
        else
        {
            this->EndDialogue();
        }
    }
    else if (ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS)
    {
        this->EndDialogue();
    }

    this->player->Update(this->map, dt);
    this->UpdateSystem(dt);

    /// If not engaged in a talk with an alien, see if a new alien should be selected based on proximity
    if (!this->alienSelected || (this->alienSelected && !this->alienSelected->talking))
    {
        this->alienSelected = NULL;
        for (int i = 0; i < MAX_ALIENS; i++)
        {
            this->arrAliens[i].Update(this->map, dt);

            if (int((this->player->x + 32) / 64) == int((this->arrAliens[i].x + 32) / 64) &&
                int((this->player->y + 68) / 64) == int((this->arrAliens[i].y + 68) / 64))
            {
                this->alienSelected = &this->arrAliens[i];
            }
        }
    }
    else
    {
        for (int i = 0; i < MAX_ALIENS; i++)
        {
            this->arrAliens[i].Update(this->map, dt);
        }
    }
}



void LabScreen::Draw()
{
    if(!this->inTheLab && !this->inTheEV) 
    {
        this->map->Draw(0, 0);
        for (int i = 0; i < MAX_ALIENS; i++)
        {
            if (this->alienSelected == &this->arrAliens[i])
            {
                this->arrAliens[i].Draw(true);
            }
            else
            {
                this->arrAliens[i].Draw(false);
            }
        }
    
        this->player->Draw();
    }
    
}

void LabScreen::DrawOnGUI()
{
    Game *game = NULL;
    for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++) 
    {
        this->arrPathogenSlots[i].Draw();
    }

    if(this->inTheLab) {
        for(int i = 0; i < MAX_PATHOGENS_AVAILABLE; i++) 
        {
            this->arrAvailablePathogenSlots[i].Draw();
            
        }
        this->slotA->Draw();
        this->slotB->Draw();
    }

    if(this->mergingPathogens) {
        this->pathogenC->Draw();

        if(this->pathogenA->x == this->pathogenB->x) {
            this->pathogenA->alpha -= 0.01f;
            this->pathogenB->alpha -= 0.01f;

            al_set_target_bitmap(this->imgC);
            int x= arc4random() % 27;
            int y= arc4random() % 27;
            if(this->quads[0] == 0) {
                al_draw_bitmap_region(this->imgA, x, y, 6, 6, x, y, 0);
            }
            else {
                al_draw_bitmap_region(this->imgB, x, y, 6, 6, x, y, 0);
            }

            x= arc4random() % 27 + 32;
            y= arc4random() % 27;
            if(this->quads[1] == 0) {
                al_draw_bitmap_region(this->imgA, x, y, 6, 6, x, y, 0);
            }
            else {
                al_draw_bitmap_region(this->imgB, x, y, 6, 6, x, y, 0);
            }

            x= arc4random() % 27;
            y= arc4random() % 27 + 32;
            if(this->quads[2] == 0) {
                al_draw_bitmap_region(this->imgA, x, y, 6, 6, x, y, 0);
            }
            else {
                al_draw_bitmap_region(this->imgB, x, y, 6, 6, x, y, 0);
            }

            x= arc4random() % 27 + 32;
            y= arc4random() % 27 + 32;
            if(this->quads[3] == 0) {
                al_draw_bitmap_region(this->imgA, x, y, 6, 6, x, y, 0);
                
            }
            else {
                al_draw_bitmap_region(this->imgB, x, y, 6, 6, x, y, 0);
                
            }
            al_set_target_bitmap(al_get_backbuffer(game->getInstance().display));
        }
        else {
            this->pathogenA->Draw();
            this->pathogenB->Draw();
            
        }

    }




    if(this->inTheEV) {
        for(int i = 0; i < MAX_PATHOGENS_AVAILABLE; i++) 
        {
            this->arrAvailablePathogenSlots[i].Draw();   
        }
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private Functions

void LabScreen::StartDialogue()
{
    if (this->alienSelected)
    {
        this->dialogueBox->x = this->alienSelected->x + 50;
        this->dialogueBox->y = this->alienSelected->y - 100;
        if (this->dialogueBox->x > 600)
        {
            this->dialogueBox->x = 600;
        }
        if (this->dialogueBox->x < 100)
        {
            this->dialogueBox->x = 100;
        }
        if (this->dialogueBox->y > 600)
        {
            this->dialogueBox->y = 600;
        }
        if (this->dialogueBox->y < 100)
        {
            this->dialogueBox->y = 100;
        }

        wz_show(this->dialogueBox, 1);

        this->dialogueMessage->text = al_ustr_new(this->alienSelected->msg.c_str());
        string str = to_string(this->alienSelected->health->gut) + "\n" + to_string(this->alienSelected->health->heart) + "\n" + to_string(this->alienSelected->health->brain) + "\n" + to_string(this->alienSelected->health->temp);
        this->wzVitals->text = al_ustr_new(str.c_str());

        this->alienSelected->talking = true;
    }
}

void LabScreen::EndDialogue()
{
    if (this->alienSelected && this->alienSelected->talking)
    {
        this->alienSelected->talking = false;
        wz_show(this->dialogueBox, 0);
    }
}

void LabScreen::BalanceSystem()
{
    /// Max 32
    /// Min 8
    int bucketGut = arc4random() % 8 + 2;
    int bucketHeart = arc4random() % 8 + 2;
    int bucketBrain = arc4random() % 8 + 2;
    int bucketTemp = arc4random() % 8 + 2;

    int negGut = bucketGut;
    int negHeart = bucketHeart;
    int negBrain = bucketBrain;
    int negTemp = bucketTemp;

    int posGut = bucketGut;
    int posHeart = bucketHeart;
    int posBrain = bucketBrain;
    int posTemp = bucketTemp;

    fprintf(stderr, "Buckets:  %d %d %d %d\n", bucketGut, bucketHeart, bucketBrain, bucketTemp);

    vector<Pathogen *> arrPathogens;
    int countPathogens = 0;
    bool system_balanced = false;
    int buckets[MAX_HFACTORS] = {negGut, negHeart, negBrain, negTemp, posGut, posHeart, posBrain, posTemp};
    int buckets_impacted = arc4random() % 3 + 1;
    int buckets_impacted_tracker = 0;
    Pathogen *newPathogen = new Pathogen();

    while (countPathogens < MAX_PATHOGENS_IN_SYSTEM && !system_balanced)
    {
        
        vector<int> available_buckets;

        /// It's possible all buckets have been cleared before the 8 balanced pathogens are created
        system_balanced = true;
        for (int i = 0; i < MAX_HFACTORS; i++)
        {
            if (buckets[i] != 0)
            {
                system_balanced = false;
                available_buckets.push_back(i);
            }
        }

        /// We've used up all of the buckets, no more pathogens are needed
        if (!system_balanced)
        {
            /// Now, balance the system
            if (countPathogens == MAX_PATHOGENS_IN_SYSTEM - 1)
            {
                /// This is the last Pathogen possible.  This one needs to finish out balancing the system.
                for (int i = 0; i < MAX_HFACTORS; i++)
                {
                    newPathogen->health->Adjust(i, buckets[i]);
                }
            }
            else
            {
                /// This is not the last Pathogen.  It's okay if he doesn't quite balance the system.
                int bucket_chosen = available_buckets.at(arc4random() % available_buckets.size());
                int bucket_size = buckets[bucket_chosen];
                int bucket_pull = arc4random() % bucket_size + 1;

                newPathogen->health->Adjust(bucket_chosen, bucket_pull);
                buckets[bucket_chosen] -= bucket_pull;
            }

            /// Give each new Pathogen an opportunity to impact multiple things
            if (buckets_impacted_tracker >= buckets_impacted || countPathogens == MAX_PATHOGENS_IN_SYSTEM - 1)
            {
                arrPathogens.push_back(newPathogen);

                newPathogen = new Pathogen();
                countPathogens++;
                buckets_impacted_tracker = 0;
                buckets_impacted = arc4random() % 3 + 1;
            }
            else
            {
                buckets_impacted_tracker++;
            }
        }
        else {
            /// Make sure the last pathogen gets added on!
            arrPathogens.push_back(newPathogen);
        }
    }

    int i = 0;
    fprintf(stderr, "\n\n");
    int img[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    for (vector<Pathogen *>::iterator it = arrPathogens.begin(); it != arrPathogens.end(); ++it)
    {
        
        (*it)->Print();

        int choose = arc4random() % PATHOGEN_TYPES;
        while(img[choose] == 1) {
            choose = arc4random() % PATHOGEN_TYPES;
        }

        (*it)->LoadImage(choose);
        img[choose] = 1;

        
        if(i < 2) {
            this->arrPathogenSlots[i].pathogen = (*it);
        }
        
        this->arrAvailablePathogenSlots[i].pathogen = (*it);
        
        i++;
    }

}


void LabScreen::UpdateSystem(double dt)
{
    this->timer_health += dt;

    Health *impactedHealth = new Health(0);
    for(int i = 0; i < MAX_PATHOGENS_IN_SYSTEM; i++)
    {
        if(this->arrPathogenSlots[i].pathogen) {
            Pathogen *p = this->arrPathogenSlots[i].pathogen;
            impactedHealth->gut += p->health->gut;
            impactedHealth->heart += p->health->heart;
            impactedHealth->brain += p->health->brain;
            impactedHealth->temp += p->health->temp;
        }
    }

    if(this->timer_health > 5.0f) {
        this->timer_health = 0;
        this->health->Impact(H_GUT, impactedHealth->gut);
        this->health->Impact(H_HEART, impactedHealth->heart);
        this->health->Impact(H_BRAIN, impactedHealth->brain);
        this->health->Impact(H_TEMP, impactedHealth->temp);
    }

    
    this->strGutStatus = DetermineStatus(this->health->gut, impactedHealth->gut);
    this->strHeartStatus = DetermineStatus(this->health->heart, impactedHealth->heart);
    this->strBrainStatus = DetermineStatus(this->health->brain, impactedHealth->brain);
    this->strTempStatus = DetermineStatus(this->health->temp, impactedHealth->temp);

    
    al_ustr_free(this->wzSystem->text);
    string str = this->strGutStatus + "\n" + this->strHeartStatus + "\n" + this->strBrainStatus + "\n" + this->strTempStatus;
    this->wzSystem->text = al_ustr_new(str.c_str());

}

string LabScreen::DetermineStatus(int currentFactor, int changeInFactor) {
    if(currentFactor > 90) {
        return "Normal";
    }
    else if(currentFactor > 50 && changeInFactor < 0) {
        return "Decaying";
    }
    else if(currentFactor > 30 && changeInFactor < 0) {
        return "Danger";
    }
    else if(currentFactor >= 0 && changeInFactor < 0) {
        return "Critical";
    }
    else if(changeInFactor > 0) {
        return "Improving";
    }

    return "Unknown";
}

void LabScreen::Pause(ALLEGRO_EVENT ev, double dt) {
    Game *game = NULL;
    if (ev.type == WZ_BUTTON_PRESSED)
    {
        switch ((int)ev.user.data1)
        {
        case BUTTON_PAUSE:
            this->paused = !this->paused;
            if(this->paused)
            {
                game->getInstance().PlaySong(2);
            }
            else
            {
                game->getInstance().PlaySong(1);
            }
            
            break;
        }
    }

    this->timer_dance += dt;
    if(this->timer_dance > this->timer_dance_duration) {
        this->timer_dance = 0;
        this->dance_step_ndx++;
        if(this->dance_step_ndx >= MAX_DANCE_STEPS){
            this->dance_step_ndx = 0;
        }

        this->timer_dance_duration = dance_steps[this->dance_step_ndx];

        this->player->flip = !this->player->flip;
        for(int i = 0; i < MAX_ALIENS; i++) {
            this->arrAliens[i].flip = !this->arrAliens[i].flip;
        }


        
    }


}

void LabScreen::MergePathogens()
{
    Game *game = NULL;

    al_set_target_bitmap(this->imgA);
    al_clear_to_color(al_map_rgba_f(0, 0, 0, 0));
    al_draw_scaled_bitmap(this->slotA->pathogen->image, 0, 0, 64, 64, 0, 0, 64, 64, 0);
    this->pathogenA = new Pathogen;
    this->pathogenA->image = this->imgA;
    this->pathogenA->x = this->slotA->x;
    this->pathogenA->y = this->slotA->y;

    al_set_target_bitmap(this->imgB);
    al_clear_to_color(al_map_rgba_f(0, 0, 0, 0));
    al_draw_scaled_bitmap(this->slotB->pathogen->image, 0, 0, 64, 64, 0, 0, 64, 64, 0);
    this->pathogenB = new Pathogen;
    this->pathogenB->image = this->imgB;
    this->pathogenB->x = this->slotB->x;
    this->pathogenB->y = this->slotB->y;

    this->pathogenC = new Pathogen;
    this->pathogenC->image = this->imgC;
    this->pathogenC->x = 370;
    this->pathogenC->y = 200;


    al_set_target_bitmap(al_get_backbuffer(game->getInstance().display));

    this->quads[0] = arc4random() % 2;
    this->quads[1] = arc4random() % 2;
    this->quads[2] = arc4random() % 2;
    this->quads[3] = arc4random() % 2;

    if(arc4random() % 100 > 50) {
        this->pathogenC->health->gut = this->slotA->pathogen->health->gut;
    }
    else {
        this->pathogenC->health->gut = this->slotB->pathogen->health->gut;
    }
    if(arc4random() % 100 > 50) {
        this->pathogenC->health->gut *= 2;
    }
    else {
        this->pathogenC->health->gut /= 2;
    }

    if(arc4random() % 100 > 50) {
        this->pathogenC->health->heart = this->slotA->pathogen->health->heart;
    }
    else {
        this->pathogenC->health->heart = this->slotB->pathogen->health->heart;
    }

    if(arc4random() % 100 > 50) {
        this->pathogenC->health->brain = this->slotA->pathogen->health->brain;
    }
    else {
        this->pathogenC->health->brain = this->slotB->pathogen->health->brain;
    }

    if(arc4random() % 100 > 50) {
        this->pathogenC->health->temp = this->slotA->pathogen->health->temp;
    }
    else {
        this->pathogenC->health->temp = this->slotB->pathogen->health->temp;
    }


    if(arc4random() % 100 > 50) {
        this->pathogenC->health->heart *= 2;
    }
    else {
        this->pathogenC->health->heart /= 2;
    }


    if(arc4random() % 100 > 50) {
        this->pathogenC->health->brain *= 2;
    }
    else {
        this->pathogenC->health->brain /= 2;
    }


    if(arc4random() % 100 > 50) {
        this->pathogenC->health->temp *= 2;
    }
    else {
        this->pathogenC->health->temp /= 2;
    }

    this->slotA->pathogen = NULL;
    this->slotB->pathogen = NULL;
    
}
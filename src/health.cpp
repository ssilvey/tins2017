//
//	Health.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#include "health.h"
#include "game.h"

Health::Health()
{
    this->gut = 100;
    this->heart = 100;
    this->temp = 100;
    this->brain = 100;
}

Health::Health(int value)
{
    this->gut = value;
    this->heart = value;
    this->temp = value;
    this->brain = value;
}

Health::~Health()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public Methods

void Health::Adjust(int factor, int amount)
{
    switch (factor)
    {
    case NEG_GUT:
        this->gut -= amount;
        break;
    case NEG_HEART:
        this->heart -= amount;
        break;
    case NEG_BRAIN:
        this->brain -= amount;
        break;
    case NEG_TEMP:
        this->temp -= amount;
        break;
    case POS_GUT:
        this->gut += amount;
        break;
    case POS_HEART:
        this->heart += amount;
        break;
    case POS_BRAIN:
        this->brain += amount;
        break;
    case POS_TEMP:
        this->temp += amount;
        break;
    }
}

void Health::Impact(int factor, int amount)
{
    switch (factor)
    {
    case H_GUT:
        this->gut += amount;
        break;
    case H_HEART:
        this->heart += amount;
        break;
    case H_BRAIN:
        this->brain += amount;
        break;
    case H_TEMP:
        this->temp += amount;
        break;
    }

    if(this->gut < 0) {
        this->gut = 0;
    }
    if(this->gut > 100) {
        this->gut = 100;
    }
    if(this->heart < 0) {
        this->heart = 0;
    }
    if(this->heart > 100) {
        this->heart = 100;
    }
    if(this->brain < 0) {
        this->brain = 0;
    }
    if(this->brain > 100) {
        this->brain = 100;
    }
    if(this->temp < 0) {
        this->temp = 0;
    }
    if(this->temp > 100) {
        this->temp = 100;
    }
}

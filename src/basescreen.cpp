//
//	BaseScreen.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#include "basescreen.h"
#include "game.h"

BaseScreen::BaseScreen()
{
}

void BaseScreen::Setup()
{
}

void BaseScreen::Update(ALLEGRO_EVENT ev, double dt)
{
    Game *game = NULL;
    if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
    {

        switch (ev.keyboard.keycode)
        {
        case ALLEGRO_KEY_F4:
            game->getInstance().SwitchScreenMode();
            break;
        case ALLEGRO_KEY_ESCAPE:
            game->getInstance().done = true;
            break;
        case ALLEGRO_KEY_UP:
            game->getInstance().key[KEY_UP] = true;
            break;

        case ALLEGRO_KEY_DOWN:
            game->getInstance().key[KEY_DOWN] = true;
            break;

        case ALLEGRO_KEY_LEFT:
            game->getInstance().key[KEY_LEFT] = true;
            break;
        case ALLEGRO_KEY_RIGHT:
            game->getInstance().key[KEY_RIGHT] = true;
            break;
        case ALLEGRO_KEY_LSHIFT:
            game->getInstance().key[KEY_SHIFT] = true;
            break;
        case ALLEGRO_KEY_SPACE:
            game->getInstance().key[KEY_SPACE] = true;
            break;

        default:
            break;
        }
    }
    else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
    {
        game->getInstance().done = true;
    }
    else if (ev.type == ALLEGRO_EVENT_KEY_UP)
    {
        switch (ev.keyboard.keycode)
        {
        case ALLEGRO_KEY_UP:
            game->getInstance().key[KEY_UP] = false;
            break;

        case ALLEGRO_KEY_DOWN:
            game->getInstance().key[KEY_DOWN] = false;
            break;

        case ALLEGRO_KEY_LEFT:
            game->getInstance().key[KEY_LEFT] = false;
            break;

        case ALLEGRO_KEY_RIGHT:
            game->getInstance().key[KEY_RIGHT] = false;
            break;
        case ALLEGRO_KEY_LSHIFT:
            game->getInstance().key[KEY_SHIFT] = false;
            break;
        case ALLEGRO_KEY_SPACE:
            game->getInstance().key[KEY_SPACE] = false;
            break;
        }
    }

    if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
    {
        if (ev.joystick.button == 1)
        {
            game->getInstance().key[KEY_SPACE] = true;
        }
        else if (ev.joystick.button == 2)
        {
            game->getInstance().key[KEY_SHIFT] = true;
        }

        
        
    }
    else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP)
    {
        if (ev.joystick.button == 1)
        {
            game->getInstance().key[KEY_SPACE] = false;
        }
        else if (ev.joystick.button == 2)
        {
            game->getInstance().key[KEY_SHIFT] = false;
        }
    }
    if(ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS) {
        /// left:  0 -1
        /// down:  1  1
        /// right  0  1
        /// up:  1 -1
        
        if(ev.joystick.axis == 0) {
            if(ev.joystick.pos < -0.1) {
                game->getInstance().key[KEY_LEFT] = true;
            }
            else if(ev.joystick.pos > 0.1) {
                game->getInstance().key[KEY_RIGHT] = true;
            }
            else {
                game->getInstance().key[KEY_RIGHT] = false;
                game->getInstance().key[KEY_LEFT] = false;
            }
            
        }
        else if(ev.joystick.axis == 1) {
            if(ev.joystick.pos < -0.1) {
                game->getInstance().key[KEY_UP] = true;
            }
            else if(ev.joystick.pos > 0.1) {
                game->getInstance().key[KEY_DOWN] = true;
            }
            else {
                game->getInstance().key[KEY_UP] = false;
                game->getInstance().key[KEY_DOWN] = false;
            }
        }
    }

    

}

void BaseScreen::Draw()
{

}

void BaseScreen::DrawOnGUI()
{

}
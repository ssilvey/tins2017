//
//	PathogenSlot.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#include "pathogenslot.h"

PathogenSlot::PathogenSlot()
{

    this->imgSlot = al_load_bitmap("./resources/sprites/tile.png");
    if (!this->imgSlot)
    {
        fprintf(stderr, "failed to load ./resources/sprites/tile.png!\n");
    }

    this->pathogen = NULL;
    this->big = false;
    this->selected = false;
    this->selectable = false;
}

PathogenSlot::~PathogenSlot()
{
    if (imgSlot)
    {
        al_destroy_bitmap(this->imgSlot);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public Functions
void PathogenSlot::Draw()
{
    int size = 40;
    if (this->big)
    {
        size = 64;
    }
    if(this->selected){
        al_draw_tinted_scaled_bitmap(this->imgSlot, al_map_rgba_f(0.95,0.35,0, 1.0), 0, 0, 64, 64, this->x, this->y, size, size, 0);
    }
    else {
        al_draw_scaled_bitmap(this->imgSlot, 0, 0, 64, 64, this->x, this->y, size, size, 0);
    }
    
    
    if (this->pathogen)
    {
        if(this->selected){
            al_draw_tinted_scaled_bitmap(this->pathogen->image, al_map_rgba_f(0.95,0.35,0, 1.0), 0, 0, 64, 64, this->x, this->y, size, size, 0);
        }
        else {
            al_draw_scaled_bitmap(this->pathogen->image, 0, 0, 64, 64, this->x, this->y, size, size, 0);
        }
    }
}

void PathogenSlot::Update(ALLEGRO_EVENT ev, double dt)
{
    int size = 40;
    if (this->big)
    {
        size = 64;
    }
    if (this->selectable)
    {
        if (ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
            ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY)
        {

            if (ev.mouse.x > this->x && ev.mouse.x < this->x + size &&
                ev.mouse.y > this->y && ev.mouse.y < this->y + size)
            {
                this->selected = true;
            }
            else {
                this->selected = false;
            }
        }
        
    }
    
}
//
//  setup.cpp
//  TINS2017 - tsl7
//  Created by Steven Silvey
//  October 20th, 2017
//

#include "allegro5/allegro.h"
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_font.h"
#include "allegro5/allegro_ttf.h"
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "widgetz/widgetz.h"
#include <stdio.h>
#include <math.h>

#include "setup.h"
#include "constants.h"
#include "game.h"
#include "titlescreen.h"

int setup()
{

    ALLEGRO_DISPLAY *display = NULL;
    if (!al_init())
    {
        fprintf(stderr, "failed to initialize allegro!\n");
        return -1;
    }

    al_set_new_display_flags(ALLEGRO_RESIZABLE);
    display = al_create_display(SCREEN_W, SCREEN_H);
    if (!display)
    {
        fprintf(stderr, "failed to create display!\n");
        return -1;
    }

    if (!al_install_keyboard())
    {
        fprintf(stderr, "failed to initialize the keyboard!\n");
        al_destroy_display(display);
        return -1;
    }

    if (!al_init_font_addon())
    {
        fprintf(stderr, "failed to initialize font addon!\n");
        al_destroy_display(display);

        return -1;
    }

    if (!al_init_ttf_addon())
    {
        fprintf(stderr, "failed to initialize font addon!\n");
        al_destroy_display(display);

        return -1;
    }

    if (!al_install_mouse())
    {
        fprintf(stderr, "failed to install the mouse!\n");
        al_destroy_display(display);
        return -1;
    }

    if (!al_init_image_addon())
    {
        fprintf(stderr, "failed to initialize the image addon!\n");
        al_destroy_display(display);

        return 0;
    }



    if (!al_install_audio())
    {
        fprintf(stderr, "failed to initialize audio!\n");
        return -1;
    }

    if (!al_init_acodec_addon())
    {
        fprintf(stderr, "failed to initialize audio codecs!\n");
        return -1;
    }

    if (!al_reserve_samples(1))
    {
        fprintf(stderr, "failed to reserve samples!\n");
        return -1;
    }

    
    
    








    Game *game = NULL;
    if (!game->getInstance().Setup(display))
    {
        al_destroy_display(display);
        return -1;
    }

    game->getInstance().titleScreen = new TitleScreen();
    game->getInstance().titleScreen->Setup();
    game->getInstance().gui = game->getInstance().titleScreen->gui;
    game->getInstance().currentScreen = game->getInstance().titleScreen;

    game->getInstance().labScreen = new LabScreen();
    game->getInstance().labScreen->Setup();

    game->getInstance().Run();
    game->getInstance().Cleanup();

    al_destroy_display(display);
    al_shutdown_ttf_addon();
    al_shutdown_font_addon();
    al_uninstall_mouse();
    al_uninstall_keyboard();
    al_uninstall_audio();

    al_uninstall_system();

    return 0;
}

//
//	Map.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#include "map.h"
#include "constants.h"

Map::Map()
{

}

Map::Map(const char *path, const char *filename)
{
    this->aMap = al_open_map(path, filename);
    if (!this->aMap)
    {
        fprintf(stderr, "Unable to load sl_room1.tmx from the resources folder!");
    }
}

bool Map::isTileValid(int x, int y)
{
    if(x >= MAX_TILE_COLUMNS || x < 0 || y >= MAX_TILE_ROWS || y < 0) {
        return false;
    }
    
    ALLEGRO_MAP_LAYER *layer = NULL;
    char *cstr = new char(20);
    strcpy(cstr, "Tile Layer 1");

    
    layer = al_get_map_layer(this->aMap, cstr);
    char c;
    c = al_get_single_tile_id(layer, x, y);
    
    if(c == 9 || c == 14 || c == 15 || c == 21 || c == 22 | c == 27 || c == 29 || c == 30)
    {
        delete cstr;
        return true;
    }

    delete cstr;
    return false;
}

void Map::Draw(int x, int y)
{
    al_draw_map(this->aMap, x, y, 0);
}

ALLEGRO_MAP_TILE* Map::getTile(int x, int y)
{
    if(x >= MAX_TILE_COLUMNS || x < 0 || y >= MAX_TILE_ROWS || y < 0) {
        return NULL;
    }

    ALLEGRO_MAP_LAYER *layer = NULL;
    char *cstr = new char(20);
    strcpy(cstr, "Tile Layer 1");

    
    layer = al_get_map_layer(this->aMap, cstr);
    return al_get_single_tile(this->aMap, layer, x, y);
}
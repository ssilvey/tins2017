//
//	Alien.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#include "alien.h"
#include "game.h"
#include "map.h"
#include "health.h"

Alien::Alien()
{
    arc4random();

    string filename_walk;
    string filename_idle;

    int chance = arc4random() % 100;
    if (chance < 30)
    {
        filename_walk = "./resources/sprites/MonsterA/monA_walk.gif";
        filename_idle = "./resources/sprites/MonsterA/monA_idle.gif";
    }
    else if (chance >= 30 && chance < 70)
    {
        filename_walk = "./resources/sprites/MonsterB/monB_walk.gif";
        filename_idle = "./resources/sprites/MonsterB/monB_idle.gif";
    }
    else
    {
        filename_walk = "./resources/sprites/MonsterC/monC_walk.gif";
        filename_idle = "./resources/sprites/MonsterC/monC_idle.gif";
    }

    this->avatar_walk = algif_load_animation(filename_walk.c_str());
    if (!this->avatar_walk)
    {
        string msg = "Unable to load " + filename_walk + " from the resources folder!";
        ;
        fprintf(stderr, "%s", msg.c_str());
    }

    this->avatar_idle = algif_load_animation(filename_idle.c_str());
    if (!this->avatar_idle)
    {
        string msg = "Unable to load " + filename_idle + " from the resources folder!";
        ;
        fprintf(stderr, "%s", msg.c_str());
    }

    this->x = arc4random() % 600 + 20;
    this->y = arc4random() % 600 + 20;
    this->flip = false;
    this->dir = DIR_NONE;
    this->dir_duration = 0;
    this->dir_timer = 0;
    this->idle = true;
    this->talking = false;

    this->msg = "Hello world";
    int i = arc4random() % 100;
    if(i > 90) {
        this->msg = "There was not enough time!";
    }
    else if (i > 80) {
        this->msg = "I don't want to be sick";
    }
    else if (i > 70) {
        this->msg = "This game wasn't playtested...";
    }
    else if (i > 30) {
        this->msg = "Bugs everywhere...in the game!";
    }

    this->health = new Health();
}

Alien::~Alien()
{
    algif_destroy_animation(this->avatar_idle);
    algif_destroy_animation(this->avatar_walk);
    delete this->health;
}

void Alien::Update(Map *map, double dt)
{
    double oldx = this->x;
    double oldy = this->y;
    int dir_x = arc4random() % 3 - 1;
    int dir_y = arc4random() % 3 - 1;

    this->dir_timer += dt;

    /// Time to head in a new direction
    if (this->dir_timer > this->dir_duration)
    {
        this->dir_timer = 0;
        this->dir = arc4random() % 5;
        this->dir_duration = (arc4random() % 400 + 100) / (arc4random() % 100 + 100) + 1;
    }

    if(this->talking) {
        this->dir = DIR_NONE;
    }

    /// Move in the direction the alien has randomly decided upon
    this->idle = false;
    switch (this->dir)
    {
    case DIR_NONE:
        dir_x = 0;
        dir_y = 0;
        this->idle = true;
        break;
    case DIR_DOWN:
        dir_x = 0;
        dir_y = 1;
        break;
    case DIR_LEFT:
        dir_x = -1;
        dir_y = 0;
        break;
    case DIR_RIGHT:
        dir_x = 1;
        dir_y = 0;
        break;
    case DIR_UP:
        dir_x = 0;
        dir_y = -1;
        break;
    }

    /// Determine which direction the avatar is facing
    if (dir_x >= 0)
    {
        this->flip = true;
    }
    else
    {
        this->flip = false;
    }

    /// Now Move
    double speed = 64;
    double move_x = dir_x * speed * dt;
    double move_y = dir_y * speed * dt;
    this->x += move_x;
    this->y += move_y;

    /// Make sure they moved onto a valid spot
    if (!map->isTileValid((this->x + 8) / 64, (this->y + 68) / 64))
    {
        this->x = oldx;
        this->y = oldy;
    }
    if (!map->isTileValid((this->x + 32) / 64, (this->y + 68) / 64))
    {
        this->x = oldx;
        this->y = oldy;
    }
    if (!map->isTileValid((this->x + 32) / 64, (this->y + 44) / 64))
    {
        this->x = oldx;
        this->y = oldy;
    }
    if (!map->isTileValid((this->x + 32) / 64, (this->y + 68) / 64))
    {
        this->x = oldx;
        this->y = oldy;
    }

    if (this->x == oldx && this->y == oldy)
    {
        this->idle = true;
    }

}

void Alien::Draw(bool selected)
{
    int flags = 0;
    if (this->flip)
    {
        flags = ALLEGRO_FLIP_HORIZONTAL;
    }

    if(selected) {
        if (this->idle)
        {
            al_draw_tinted_bitmap(algif_get_bitmap(this->avatar_idle, al_get_time()), al_map_rgba_f(0.95,0.95,0, 1.0),  this->x, this->y, flags);
        }
        else
        {
            al_draw_tinted_bitmap(algif_get_bitmap(this->avatar_walk, al_get_time()), al_map_rgba_f(0.95,0.95,0, 1.0),  this->x, this->y, flags);
        }
    }
    else {
        if (this->idle)
        {
            al_draw_bitmap(algif_get_bitmap(this->avatar_idle, al_get_time()), this->x, this->y, flags);
        }
        else
        {
            al_draw_bitmap(algif_get_bitmap(this->avatar_walk, al_get_time()), this->x, this->y, flags);
        }
    }
    

    
}
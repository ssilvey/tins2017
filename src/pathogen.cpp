//
//	pathogen.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 22nd, 2017
//

#include "pathogen.h"
#include <stdio.h>
#include <string>
using namespace std;

Pathogen::Pathogen()
{
    this->health = new Health;
    this->health->gut = 0;
    this->health->heart = 0;
    this->health->brain = 0;
    this->health->temp = 0;
    this->alpha = 1.0f;
}

Pathogen::~Pathogen()
{
    al_destroy_bitmap(this->image);
}

void Pathogen::Print()
{
    fprintf(stderr, "Pathogen--  gut:%d heart:%d brain:%d temp:%d \n", this->health->gut, this->health->heart, this->health->brain, this->health->temp);
}

void Pathogen::LoadImage(int ndx)
{
    string filename = "./resources/sprites/pathogens/marbles-" + to_string(ndx+1) + ".png";
    this->image = al_load_bitmap(filename.c_str());
    if (!this->image)
    {
        string msg = "failed to load " + filename + "!\n";
        fprintf(stderr, "%s", msg.c_str());
    }
}

void Pathogen::Draw()
{
    al_draw_tinted_bitmap(this->image, al_map_rgba_f(1, 1, 1, this->alpha), this->x, this->y, 0);
}


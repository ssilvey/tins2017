//
//	Player.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 21st, 2017
//

#include "player.h"
#include "game.h"
#include <string>
using namespace std;


Player::Player()
{
    this->avatar_idle = algif_load_animation("./resources/sprites/alien_idle.gif");
    if (!this->avatar_idle)
    {
        fprintf(stderr, "Unable to load alien_idle.gif from the resources folder!");
    }

    this->avatar_running = algif_load_animation("./resources/sprites/alien_run.gif");
    if (!this->avatar_running)
    {
        fprintf(stderr, "Unable to load alien_run.gif from the resources folder!");
    }

    this->avatar_running_fast = algif_load_animation("./resources/sprites/alien_run_fast.gif");
    if (!this->avatar_running_fast)
    {
        fprintf(stderr, "Unable to load alien_run_fast.gif from the resources folder!");
    }


    this->x = 64;
    this->y = 616;
    this->flip = false;
    this->running = false;
}

Player::~Player()
{
    algif_destroy_animation(this->avatar_idle);
    algif_destroy_animation(this->avatar_running);
    algif_destroy_animation(this->avatar_running_fast);
}

void Player::Update(Map *map, double dt)
{
    Game *game = NULL;
    double oldx = this->x;
    double oldy = this->y;
    double speed = 128 * dt;
    this->fast = false;
    if(game->getInstance().key[KEY_SHIFT])
    {
        speed *= 2.0f;
        this->fast = true;
    }

    

    if (game->getInstance().key[KEY_UP])
    {
        this->y -= speed;
        if(!map->isTileValid((this->x+16) / 64, (this->y+64) / 64)) 
        {
            this->y = oldy;
        }
    }
    if (game->getInstance().key[KEY_DOWN])
    {
        this->y += speed;
        if(!map->isTileValid((this->x+16) / 64, (this->y+84) / 64)) 
        {
            this->y = oldy;
        }
    }
    if (game->getInstance().key[KEY_RIGHT])
    {
        this->x += speed;
        this->flip = false;

        if(!map->isTileValid((this->x+48) / 64, (this->y+84) / 64)) 
        {
            this->x = oldx;
        }
    }
    if (game->getInstance().key[KEY_LEFT])
    {
        this->x -= speed;
        this->flip = true;

        if(!map->isTileValid((this->x+16) / 64, (this->y+84) / 64)) 
        {
            this->x = oldx;
        }
    }
    
    if(oldx == this->x && oldy == this->y) {
        this->running = false;
    }
    else {
        this->running = true;
    }
    
    
}

void Player::Draw()
{
    int flags = 0;
    if(this->flip) {
        flags = ALLEGRO_FLIP_HORIZONTAL;
    }
    if(this->running) {
        if(this->fast) {
            al_draw_bitmap(algif_get_bitmap(this->avatar_running_fast, al_get_time()), this->x, this->y, flags);    
        }
        else {
            al_draw_bitmap(algif_get_bitmap(this->avatar_running, al_get_time()), this->x, this->y, flags);
        }
    }
    else {
        al_draw_bitmap(algif_get_bitmap(this->avatar_idle, al_get_time()), this->x, this->y, flags);
    }
    
}

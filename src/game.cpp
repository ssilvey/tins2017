//
//  game.cpp
//  eldersofethos
//
//  Created by Steven Silvey on 6/26/15.
//
//

#include "game.h"
#include "titlescreen.h"


#include <ctype.h>
#include <math.h>
#include <allegro5/allegro_primitives.h>
#include "allegro5/allegro_native_dialog.h"
#include "allegro5/allegro_font.h"
#include "allegro5/allegro_ttf.h"


int Game::Setup(ALLEGRO_DISPLAY *display)
{
    
    this->guisize = 1.0f;
    this->timer = al_create_timer(1.0 / FPS);
    if (!this->timer)
    {
        fprintf(stderr, "failed to create timer!\n");
        return 0;
    }

    this->baseFont = al_load_ttf_font("./resources/fonts/arial.ttf", 18, 0);
    if (!this->baseFont)
    {
        fprintf(stderr, "Could not load 'arial.ttf'.\n");
        al_destroy_timer(this->timer);
        return 0;
    }

    this->joystick = NULL;
    if (!al_install_joystick())
    {
        fprintf(stderr, "failed to initialize the joystick!\n");
    }
    al_reconfigure_joysticks();
    this->joystick = al_get_joystick(al_get_num_joysticks() - 1);


    this->event_queue = al_create_event_queue();
    if (!this->event_queue)
    {
        fprintf(stderr, "failed to create event_queue!\n");
        al_destroy_timer(this->timer);
        return 0;
    }


    this->song1 = al_load_sample("./resources/audio/Intro_Screen.ogg");
    if (!this->song1)
    {
        printf("Audio clip sample not loaded!\n");
        return -1;
    }

    

    this->song2 = al_load_sample("./resources/audio/Percussion_Groove.ogg");
    if (!this->song1)
    {
        printf("Audio clip sample not loaded!\n");
        return -1;
    }
    this->song3 = al_load_sample("./resources/audio/Minimal_Funky_Loop.ogg");
    if (!this->song1)
    {
        printf("Audio clip sample not loaded!\n");
        return -1;
    }

    al_play_sample(this->song1, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);

    this->display = display;
    al_register_event_source(this->event_queue, al_get_display_event_source(this->display));
    al_register_event_source(this->event_queue, al_get_timer_event_source(this->timer));
    al_register_event_source(this->event_queue, al_get_keyboard_event_source());
    al_register_event_source(this->event_queue, al_get_mouse_event_source());
    if(this->joystick) {
        al_register_event_source(this->event_queue, al_get_joystick_event_source());
    }
    

    al_start_timer(this->timer);

    this->key[KEY_UP] = false;
    this->key[KEY_DOWN] = false;
    this->key[KEY_LEFT] = false;
    this->key[KEY_RIGHT] = false;

    return 1;
}

void Game::Run() 
{
    this->refresh_rate = 60;
    this->fixed_dt = 1.0f / this->refresh_rate;
    this->old_time = al_current_time();
    this->game_time = al_current_time();

    this->redraw = true;
    this->done = false;

    while (!this->done)
    {
        Update();
        Draw();

        
    }
}

void Game::Draw()
{
    if (this->redraw && al_is_event_queue_empty(this->event_queue))
    {
        this->redraw = false;

        al_clear_to_color(al_map_rgb(0, 0, 0));
                
        /// Draw code here
        this->currentScreen->Draw();
        wz_draw(this->currentScreen->gui);        
        this->currentScreen->DrawOnGUI();

        al_wait_for_vsync();
        al_flip_display();
    }
}

void Game::Update()
{
    double dt = al_current_time() - this->old_time;
    this->old_time = al_current_time();

    if (this->old_time - this->game_time > dt)
    {
        this->game_time += this->fixed_dt * floor((this->old_time - this->game_time) / this->fixed_dt);
    }

    this->start_time = al_current_time();
    this->game_time += this->fixed_dt;

    ALLEGRO_EVENT ev;
    wz_update(this->currentScreen->gui, this->fixed_dt);
    al_wait_for_event(this->event_queue, &ev);
    wz_send_event(this->currentScreen->gui, &ev);

    this->currentScreen->Update(ev, dt);
}

void Game::ProcessInput(int unichar)
{
   
}

void Game::Cleanup()
{
    wz_destroy(this->gui);
    al_destroy_timer(this->timer);
    al_destroy_event_queue(this->event_queue);

    if(this->joystick) {
        al_uninstall_joystick();
    }
}

void Game::SwitchScreenMode()
{
    /// Didn't work the way I wanted...so I commented it out.  Maybe later I can figure this out!
    // al_set_new_display_flags(ALLEGRO_FULLSCREEN);
    // al_destroy_display(this->display);
    // this->display = al_create_display(640,480);
    
}

void Game::PlaySong(int i)
{
    al_stop_samples();
    if(i == 0) {
        al_play_sample(this->song1, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
    }
    else if (i == 1) {
        al_play_sample(this->song2, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
    }
    else if (i == 2) {
        al_play_sample(this->song3, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);
    }
}

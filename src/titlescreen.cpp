//
//	TitleScreen.cpp
//	TINS2017 - tsl7
//	Created by Steven Silvey
//	October 20th, 2017
//

#include "titlescreen.h"
#include "game.h"


TitleScreen::TitleScreen()
{
}

void TitleScreen::Setup()
{
    Game *game = NULL;

    memset(&game->getInstance().theme, 0, sizeof(game->getInstance().theme));
    memcpy(&game->getInstance().theme, &wz_def_theme, sizeof(game->getInstance().theme));
    game->getInstance().theme.font = game->getInstance().baseFont;
    game->getInstance().theme.color1 = COLOR_THEME_ONE;
    game->getInstance().theme.color2 = COLOR_THEME_SIX;

    this->gui = wz_create_widget(0, 0, 0, -1);
    wz_set_theme(this->gui, (WZ_THEME *)&game->getInstance().theme);

    wz_create_textbox(this->gui, 0, 0, 300 * game->getInstance().guisize, 150 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_CENTRE, al_ustr_new("The Tragedy at Space Lab #7"), 1, -1);
    WZ_BUTTON *wz_create_button(WZ_WIDGET * parent, float x, float y, float w, float h, ALLEGRO_USTR *text, int own, int id);
    wz_create_button(this->gui, 300, 200, 200, 80, al_ustr_new("Start"), 1, 10);

    // wz_create_fill_layout(this->gui, 50 * game->getInstance().guisize, 50 * game->getInstance().guisize, 300 * game->getInstance().guisize, 450 * game->getInstance().guisize, 50 * game->getInstance().guisize, 20 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);

    // wz_create_toggle_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Toggle 1"), 1, 1, 5);
    // wz_create_toggle_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Toggle 2"), 1, 1, 6);
    // wz_create_toggle_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Toggle 3"), 1, 1, 7);
    // wz_create_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Switch Themes"), 1, 666);
    // game->getInstance().wgt = (WZ_WIDGET *)wz_create_button(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Quit"), 1, 1);
    // wz_set_shortcut(game->getInstance().wgt, ALLEGRO_KEY_Q, ALLEGRO_KEYMOD_CTRL);
    // wz_create_fill_layout(this->gui, 350 * game->getInstance().guisize, 50 * game->getInstance().guisize, 300 * game->getInstance().guisize, 450 * game->getInstance().guisize, 50 * game->getInstance().guisize, 20 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_CENTRE, al_ustr_new("Scroll Bars:"), 1, -1);
    // wz_create_scroll(this->gui, 0, 0, 200 * game->getInstance().guisize, 20 * game->getInstance().guisize, 20, 50 * game->getInstance().guisize, 9);
    // wz_create_scroll(this->gui, 0, 0, 20 * game->getInstance().guisize, 200 * game->getInstance().guisize, 20, 50 * game->getInstance().guisize, 9);
    // wz_create_scroll(this->gui, 0, 0, 20 * game->getInstance().guisize, 200 * game->getInstance().guisize, 20, 50 * game->getInstance().guisize, 9);
    // wz_create_fill_layout(this->gui, 650 * game->getInstance().guisize, 50 * game->getInstance().guisize, 300 * game->getInstance().guisize, 450 * game->getInstance().guisize, 20 * game->getInstance().guisize, 20 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_TOP, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_CENTRE, WZ_ALIGN_CENTRE, al_ustr_new("Edit Box:"), 1, -1);
    // game->getInstance().wgt = (WZ_WIDGET *)wz_create_editbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, al_ustr_new("Type here..."), 1, -1);
    // wz_create_textbox(this->gui, 0, 0, 200 * game->getInstance().guisize, 50 * game->getInstance().guisize, WZ_ALIGN_LEFT, WZ_ALIGN_TOP, al_ustr_new("A textbox with a lot of text"
    //                                                                                              " in it. Also supports new lines:\n\nNew paragraph.\n"
    //                                                                                              "Also supports unicode:\n\n"
    //                                                                                              "Привет"),
    //                   1, -1);

    wz_register_sources(this->gui, game->getInstance().event_queue);
}

void TitleScreen::Update(ALLEGRO_EVENT ev, double dt)
{
    BaseScreen::Update(ev, dt);
    Game *game = NULL;
    if (ev.type == ALLEGRO_EVENT_TIMER)
    {
        game->getInstance().redraw = true;
    }
    else if (ev.type == WZ_BUTTON_PRESSED)
    {
        switch ((int)ev.user.data1)
        {
        case 1:
        {
            break;
        }
        case 10:
        {
            
            BaseScreen *screen = game->getInstance().labScreen;
            game->getInstance().gui = screen->gui;
            game->getInstance().currentScreen = screen;
            game->getInstance().PlaySong(1);
            break;
        }
        case 666:
        {

            break;
        }
        }
    }
    
}